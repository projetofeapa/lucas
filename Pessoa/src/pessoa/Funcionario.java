package pessoa;

public class Funcionario extends Pessoa {
    
    private String senha;
    private int cod_funcioanrio;

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public int getCod_funcioanrio() {
        return cod_funcioanrio;
    }

    public void setCod_funcioanrio(int cod_funcioanrio) {
        this.cod_funcioanrio = cod_funcioanrio;
    }
    
}
