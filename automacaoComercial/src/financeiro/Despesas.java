package financeiro;

public class Despesas {

    private double gastosProduto, gastosFornecedor,
                   gastosCliente, gastosFuncionario;

    public double getGastosProduto() {
        return gastosProduto;
    }

    public void setGastosProduto(double gastosProduto) {
        this.gastosProduto = gastosProduto;
    }

    public double getGastosFornecedor() {
        return gastosFornecedor;
    }

    public void setGastosFornecedor(double gastosFornecedor) {
        this.gastosFornecedor = gastosFornecedor;
    }

    public double getGastosCliente() {
        return gastosCliente;
    }

    public void setGastosCliente(double gastosCliente) {
        this.gastosCliente = gastosCliente;
    }

    public double getGastosFuncionario() {
        return gastosFuncionario;
    }

    public void setGastosFuncionario(double gastosFuncionario) {
        this.gastosFuncionario = gastosFuncionario;
    }
    
    
}
