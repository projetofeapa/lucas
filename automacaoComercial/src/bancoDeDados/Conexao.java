package bancoDeDados;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Conexao {
    
    static Connection con;
    static Statement stm;
    static ResultSet rs;

    public static void conectar() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            System.out.println("Carregou o Driver");
            String caminho, usuario, senha;
            caminho = "jdbc:mysql://localhost:3306/bancotest";
            usuario = "root";
            senha = "";
            try {
                con = DriverManager.getConnection(caminho, usuario, senha);
                System.out.println("Conectado");
                stm = con.createStatement();
            } catch (SQLException ex) {
                System.out.println("Não Conectado");
                Logger.getLogger(Conexao.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (ClassNotFoundException ex) {
            System.out.println("Não Carregou o Driver");
            Logger.getLogger(Conexao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void manipularDados(String SQL) {
        conectar();
        try {
            stm.executeUpdate(SQL);
            System.out.println("Conseguiu Executar");
        } catch (SQLException ex) {
            System.out.println("Não Conseguiu Executar");
            Logger.getLogger(Conexao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static ResultSet retornarDados(String SQL) {
        conectar();
        try {
            rs = stm.executeQuery(SQL);
            System.out.println("Conseguiu Pesquisar!");
        } catch (SQLException ex) {
            System.out.println("Não Conseguiu Pesquisar!");
            Logger.getLogger(Conexao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rs;
    }

    public static void main(String[] args) {
        conectar();
    }
    
}
